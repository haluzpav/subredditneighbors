# Subreddit Neighbors Analyzer

This tool prints and plots a list of subreddits which are closest to some other subreddits.

[Album of results](https://imgur.com/a/POF6vDX)

![Example of /r/vegan](plot_outputs/725e2d5cc6a9bb68d6734927cb910460.png)

## How?

The data are fetched basically in 3 steps:

1. Get "hot" submissions (posts) of the provided subreddits.
2. Gather redditors (both authors and commenters) of the submissions.
3. Look where these redditors posted and commented.

The subreddits from the last step are then ordered by something I called *specificity*, which should describe how much a subreddit is specific the originally provided ones.

*specificity* = *activity* * *uniqueness*

*Activity* is relative amount of post and comments of the redditors (from step 2) in the subreddit.

*Uniqueness* is small when the subreddit has a lot of subscribers. This helps to push down large subreddits (such as /r/AskReddit) which would be on top all the time otherwise.

![uniqueness](uniqueness.png)

## Why?

Originally I lacked some kind of user specific subreddit recommendations. This tool achieves that pretty well.

I was dissatisfied with other tools such as [https://anvaka.github.io/redsim/](https://anvaka.github.io/redsim/).

## How to run

You will need Python 3.6 with libraries: 

* PRAW - for Reddit API
* jsons - for smart serialization
* matplotlib - for plotting

1. Clone this repository.
2. Fill in `login.json` file - follow [official steps](https://github.com/reddit-archive/reddit/wiki/OAuth2-Quick-Start-Example#first-steps).
3. Simply extend `batch.py` or run `python main.py --help` if you feel exploratory.

## Problems:

* Reddit API has limitation of about 1 request per second, which means it can take an **hour** when starting with big subreddits.
* This long time-frame means that state of Reddit can change a lot - redditors can get deleted, subreddits closed etc. Such anomalies are skipped.
* Automated bots are not taken into any account.
* Some redditors post on their "overview" (like Facebook wall), where other redditors can comment on it. This has no "subscribers" so I skipped this feature in data gathering. Posters of /r/gonewild seem to fancy this feature.
* The script always collects fixed number of submissions and comments of redditors, while not taking into account number of redditors or date of post etc. This could result is some statistical inconsistency between more / less active subreddits / redditors.
* Pinned posts not filtered out.

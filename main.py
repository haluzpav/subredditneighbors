import argparse
import hashlib
import os.path
from typing import List, Optional, Callable, Iterable, Dict

from dataclasses import dataclass, field

import model
from crawl import get_redditors, get_subreddits
from unique import fill_uniqueness, get_unique
from utils import assert_file_dir, format_datetime, get_now

description = 'Tool to fetch and analyze which other subreddits are popular' \
              'among redditors of selected subreddits.'
activity_reductions = {
    'max': max,
    'sum': sum,
    }


# TODO improve docs
def parse_args():
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('subreddit_names', nargs='+', type=str)
    parser.add_argument('--top', type=int, default=30)
    parser.add_argument('--activity_reduction', choices=activity_reductions.keys(), default='sum')
    parser.add_argument('--sort_by_uniqueness', action='store_true', default=True)
    parser.add_argument('--plot_uniqueness', action='store_true', default=False)
    parser.add_argument('--show_plot_window', action='store_true', default=False)
    parser.add_argument('--save_root')
    parser.add_argument('--save_name')
    args = parser.parse_args()
    args.activity_reduction = activity_reductions[args.activity_reduction]
    return args


@dataclass
class HashMap(model.Serializable):
    names: Dict[str, List[str]] = field(default_factory=dict)

    def get_and_cache(self, names: List[str], path: str):
        hashstr = self.get_names_hash(names)
        self.names[hashstr] = names
        self.save(path)
        return hashstr

    @classmethod
    def get_names_hash(cls, names: List[str]) -> str:
        names = sorted(map(lambda sn: sn.lower(), names))
        return hashlib.md5(str(names).encode('utf-8')).hexdigest()

    @classmethod
    def load(cls, path: str) -> Optional['HashMap']:
        return super().load(path)


def report(subreddits: List[model.Subreddit]):
    max_name_len = max(map(lambda s: len(s.name), subreddits))
    rank_dec_digits = 3
    print('Summary:\n' + ' '.join((
        f'{"Subreddit name": <{max_name_len+3}}',
        f'{"Visits": <6}',
        f'{"Visitors": <8}',
        f'{"Activity": <8}',
        f'{"Subscribers": <11}',
        f'{"Uniqueness": <10}',
        f'{"Specificity": <11}',
        )))
    for subreddit in subreddits:
        print(' '.join((
            f'/r/{subreddit.name: <{max_name_len}}',
            f'{subreddit.visits:6d}',
            f'{len(subreddit.visitors):8d}',
            f'{subreddit.activity:8.{rank_dec_digits}f}',
            f'{subreddit.subscribers:11d}',
            f'{subreddit.uniqueness:10.{rank_dec_digits}f}',
            f'{subreddit.specificity:11.{rank_dec_digits}f}',
            )))


def plot(
        all_subreddits: model.Subreddits,
        selected_subreddits: List[model.Subreddit],
        plot_uniqueness: bool,
        show: bool,
        path: Optional[str],
        use_tex: bool = True,
        ):
    import matplotlib.pyplot as plt
    from matplotlib import gridspec
    from matplotlib import cm

    if use_tex:
        plt.rcParams['text.usetex'] = True
        plt.rcParams['text.latex.preamble'] = r'\usepackage{bold-extra}'
    else:
        print('\tWARNING: Plot is broken by not using tex.')

    n = len(selected_subreddits)
    inds = list(range(n))
    activity = list(map(lambda s: s.activity, selected_subreddits))
    uniqueness = list(map(lambda s: s.uniqueness, selected_subreddits))
    esc_subreddit = lambda n: n.replace('_', '\_')
    format_subreddit = lambda n: f'\\texttt{{/r/{esc_subreddit(n)}}}'
    labels_origins = list(map(format_subreddit, all_subreddits.origins))
    labels = list(map(
        lambda s: f'\\textbf{{{format_subreddit(s.name)}}}' if s.name in all_subreddits.origins else format_subreddit(
            s.name), selected_subreddits))
    cmap = cm.get_cmap('viridis')
    colors = cmap(uniqueness)

    fig: plt.Figure = plt.figure(figsize=(6, 8), dpi=150)
    gs = gridspec.GridSpec(1, 1)
    ax: plt.Axes = fig.add_subplot(gs[0])
    if plot_uniqueness:
        ax.barh(inds, activity, height=-0.4, align='edge')
        ax.barh(inds, uniqueness, height=0.4, align='edge')
    else:
        ax.barh(inds, activity, color=colors)

    sm = cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(0, 1))
    sm.set_array([])
    cbar = fig.colorbar(sm, aspect=50, orientation='horizontal', pad=.09)
    cbar.set_label('Subreddit $\\textrm{{uniqueness}}$ ($\\sim\\textrm{{inverse number of subscribers}}$)', labelpad=2)

    ax.set_yticks(inds)
    ax.set_yticklabels(labels)
    ax.set_xscale('log')
    xticks_minor = sum(([x / 10 ** e for x in range(1, 10)] for e in reversed(range(4))), [])
    xticks_minor_labels = [(x if any(str(x).endswith(str(e)) for e in (0, 1, 5)) else '') for x in xticks_minor]
    ax.set_xticklabels(())
    ax.set_xticks(xticks_minor, minor=True)
    ax.set_xticklabels(xticks_minor_labels, minor=True)
    ax.set_xlim(min(activity) * .9, max(activity) * 1.1)
    ax.xaxis.grid(True, 'both', color=(0, 0, 0), alpha=.1, linewidth=1)
    ax.set_ylim(-.8, n - .2)
    ax.invert_yaxis()
    fig.suptitle(f'Top-{n} subreddits among redditors of:\n'
                 f'\\textbf{{{", ".join(labels_origins)}}}')
    ax.set_xlabel(f'Relative $\\textrm{{activity}}$ of redditors (log-scale)')
    footnote = f'Listed in decreasing order of $\\textrm{{specificity}}$ ($=\\textrm{{activity}}\\times\\textrm{{uniqueness}}$). ' \
               f'Number of redditors analyzed: {len(all_subreddits.redditors)}.\n' \
               f'Data fetched between {format_datetime(all_subreddits.data_fetch_start)} and {format_datetime(all_subreddits.data_fetch_end)}.\n' \
               f'Tools used: Python 3.6, PRAW, ramonhagenaars/jsons, matplotlib'
    fig.text(0.01, 0.01, footnote, horizontalalignment='left', fontsize=8)
    gs.tight_layout(fig, rect=(0, -.02, 1, 0.94))

    if show:
        fig.show()
    if path is not None:
        assert_file_dir(path)
        fig.savefig(path)


def run(
        subreddit_names: List[str],
        top: int = 30,
        activity_reduction: Callable[[Iterable], int] = sum,
        sort_by_uniqueness: bool = True,
        plot_uniqueness: bool = False,
        show_plot_window: bool = False,
        save_root: Optional[str] = None,
        save_name: Optional[str] = None,
        ):
    json_root = save_root or 'json_outputs'
    plot_root = save_root or 'plot_outputs'
    if not save_name:
        hashmap_path = os.path.join(json_root, '.hashmap.json')
        hashmap = HashMap.load(hashmap_path) or HashMap()
        save_name = hashmap.get_and_cache(subreddit_names, hashmap_path)
    json_path = os.path.join(json_root, f'{save_name}.json')
    plot_path = os.path.join(plot_root, f'{save_name}.png')

    print(f'Running with {subreddit_names}...\n\tInstance name: {save_name}')
    subreddits = model.Subreddits.load(json_path)
    if subreddits is None:
        print('Fetching data...')
        fetch_start = get_now()
        redditors = get_redditors(subreddit_names)
        subreddits = get_subreddits(redditors)
        subreddits.data_fetch_start = fetch_start
        subreddits.data_fetched()
        subreddits.origins = subreddit_names
        subreddits.save(json_path)
    elif subreddits.origins != subreddit_names:
        input(f'Different subreddits loaded {subreddits.origins}. Continue?')
    else:
        print('Previous results loaded.')

    if subreddits.visits is None or subreddits.reduce_function != activity_reduction.__name__:
        print('Sorting...')
        subreddits.compute_activities(activity_reduction)
        subreddits.save(json_path)

    if sort_by_uniqueness:
        print('Fetching and sorting by subreddit uniqueness...')
        selected_subreddits = get_unique(subreddits, top)
    else:
        selected_subreddits = subreddits.subreddits[:top]
    if plot_uniqueness and selected_subreddits[0].uniqueness is None:
        print('Fetching subreddit uniqueness...')
        fill_uniqueness(selected_subreddits)
        subreddits.data_fetched()
    subreddits.save(json_path)

    report(selected_subreddits)
    print('Plotting...')
    plot(subreddits, selected_subreddits, plot_uniqueness=plot_uniqueness, show=show_plot_window,
        path=plot_path)


def main():
    args = parse_args()
    run(args.subreddit_names, args.show_plot_window)


if __name__ == '__main__':
    main()

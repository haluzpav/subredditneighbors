import itertools
from typing import Set, List

import praw.models
import prawcore.exceptions

import model
from crawl_utils import get_reddit
from utils import *

reddit = get_reddit(False)

subreddit_submission_limit = 20
subreddit_comment_replace_limit = None
subreddit_comment_replace_threshold = 10
redditor_submission_limit = 10
redditor_comment_limit = 20


def get_redditors(subreddit_names: List[str]) -> Set[str]:
    names: Set[str] = set()
    for i, subreddit_name in enumerate(subreddit_names):
        print_progress(i, len(subreddit_names), postfix=f'Subreddit(/r/{subreddit_name})')
        subreddit = praw.models.Subreddit(reddit, subreddit_name)
        for j, submission in enumerate(subreddit.hot(limit=subreddit_submission_limit)):
            print_progress(j, subreddit_submission_limit,
                indent=2, postfix=f'Submission("{at_most(submission.title, 20)}")')
            names.add(submission.author.name)
            submission.comments.replace_more(
                limit=subreddit_comment_replace_limit,
                threshold=subreddit_comment_replace_threshold)
            for comment in submission.comments.list():
                if comment.author is None:
                    continue  # deleted comment
                names.add(comment.author.name)
    return names


def get_subreddits(redditors: Set[str]) -> model.Subreddits:
    subreddits: Dict[str, model.Subreddit] = {}
    for i, redditor_name in enumerate(redditors):
        print_progress(i, len(redditors), postfix=f'Redditor(/u/{redditor_name})')
        redditor = praw.models.Redditor(reddit, redditor_name)
        pukes = itertools.chain(
            redditor.submissions.new(limit=redditor_submission_limit),
            redditor.comments.new(limit=redditor_comment_limit)
            )
        try:
            for puke in pukes:
                subreddit_name = puke.subreddit.display_name
                subreddit = add_and_get(subreddits, subreddit_name, lambda: model.Subreddit(subreddit_name))
                subreddit.visits += 1
                subreddit.visitors.add(redditor_name)
        except prawcore.exceptions.Forbidden:
            print('\t\tSuspended, skipping...')
        except prawcore.exceptions.NotFound:
            print('\t\tDisappeared, skipping...')
    return model.Subreddits(redditors=redditors, subreddits=list(subreddits.values()))

import datetime
import math
import os.path
from typing import Callable, TypeVar, Dict

Tk = TypeVar('Tk')
Tv = TypeVar('Tv')


def add_and_get(dictionary: Dict[Tk, Tv], key: Tk, initializer: Callable[[], Tv]) -> Tv:
    if key in dictionary.keys():
        return dictionary[key]
    else:
        item = initializer()
        dictionary[key] = item
        return item


def get_now():
    return datetime.datetime.now()


def format_datetime(dt: datetime.datetime):
    return dt.replace(microsecond=0).astimezone(datetime.timezone.utc).isoformat(' ', )


def days_ago(timestamp: float):
    delta = datetime.datetime.utcnow() - datetime.datetime.utcfromtimestamp(timestamp)
    return delta.days


def print_progress(i: int, size: int, indent: int = 1, prefix: str = '', postfix: str = ''):
    max_digits = int(math.log10(size)) + 1
    if len(prefix) > 0:
        prefix += ' '
    prefix = '\t' * indent + prefix
    if len(postfix) > 0:
        postfix = ': ' + postfix
    print(f'{prefix}{i:{max_digits}d} / {size}{postfix}')


def at_most(string: str, max_size):
    return string if len(string) <= max_size else (string[:max_size] + '...')


def assert_file_dir(path):
    root = os.path.split(path)[0]
    if len(root) and not os.path.exists(root):
        os.makedirs(root, exist_ok=True)

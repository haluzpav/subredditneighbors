import matplotlib.pyplot as plt
import numpy as np

logx = np.arange(0, 8.1, .1)
x = np.power(10, logx)
y = (x / 1e3) ** -.25
y = np.minimum(y, 1)
# plt.plot(x, y, label='"polynomial"')

k1 = 3
k2 = 7
c = (k1 + k2) / 2
w = k2 - k1
y = 1 / (1 + np.exp(4 * (logx - c) / w))
plt.plot(x, y, label='sigmoid')

d = - (logx - c) / w + .5
plt.plot(x, d, '--', linewidth=1, label='sigmoid tangent')

plt.xscale('log')
plt.xlim(10 ** 0, 10 ** 8)
plt.ylim(0, 1)
plt.xlabel('Number of subscribers (log-scale)')
plt.ylabel('Uniqueness of a subreddit')
plt.grid(alpha=.3)
plt.legend()
# plt.show()
plt.savefig('uniqueness.png')

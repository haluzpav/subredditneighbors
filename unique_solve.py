from sympy import symbols, exp, simplify
from sympy.solvers import solve

x, r, s, k1, k2, c, w = symbols('x, r, s, k1, k2, c, w')
k_rank = 2

sig = 1 / (1 + exp(-r * (x - s)))

# (r * sig * (1 - sig)).subs(x, s) == r / 2
k1_constr = r / 2 * (k1 - s) / k_rank - .5
k2_constr = r / 2 * (k2 - s) / k_rank + .5

sol = solve([k1_constr, k2_constr], [r, s])
sol = sol[0]
print((r, s), sol)

sigk = sig.subs(((r, sol[0]), (s, sol[1])))
print(sigk)
print(simplify(sigk))

c_def = (k1 + k2) / 2 - c
w_def = k2 - k1 - w

sol = solve([c_def, w_def], [k1, k2])
print((k1, k2), sol)

sigcw = sigk.subs(((k1, sol[k1]), (k2, sol[k2])))
print(sigcw)
print(simplify(sigcw))

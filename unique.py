from queue import PriorityQueue
from typing import List

import praw.models
import prawcore.exceptions

import model
from crawl_utils import get_reddit
from utils import *

reddit = get_reddit(False)


def fill_uniqueness(subreddits: List[model.Subreddit]):
    for i, subreddit in enumerate(subreddits):
        print_progress(i, len(subreddits), postfix=f'Subreddit(/r/{subreddit.name})')
        if subreddit.name.startswith('u_'):
            print('\t\tIs redditor\'s wall. No subscribers -> no uniqueness -> skipping...')
            continue
        subreddit.subscribers = praw.models.Subreddit(reddit, subreddit.name).subscribers
        subreddit.compute_uniqueness()


def get_unique(subreddits: model.Subreddits, limit_top: int) -> List[model.Subreddit]:
    queue = PriorityQueue()
    s2q = lambda s: (- subreddit.specificity, subreddit)
    q2s = lambda q: q[1]
    for subreddit in subreddits.subreddits:
        queue.put(s2q(subreddit))
    unique: List[model.Subreddit] = []

    i = 0
    while len(unique) < limit_top:
        q = queue.get()
        subreddit: model.Subreddit = q2s(q)
        if subreddit.uniqueness is None:
            if subreddit.subscribers is None:
                print(f'\t\tChecking Subreddit(/r/{subreddit.name})')
                if subreddit.name.startswith('u_'):
                    print('\t\t\tIs redditor\'s wall. No subscribers -> no uniqueness -> skipping...')
                    continue
                try:
                    subreddit.subscribers = praw.models.Subreddit(reddit, subreddit.name).subscribers
                except prawcore.exceptions.Forbidden:
                    print('\t\tIs private.')
                    subreddit.subscribers = 1  # TODO how to handle private subreddit?
                except prawcore.exceptions.NotFound:
                    print('\t\tDisappeared.')
                    continue
                subreddits.data_fetched()
            subreddit.compute_uniqueness()
            queue.put(s2q(subreddit))
        else:
            print_progress(i, limit_top, postfix=f'Subreddit(/r/{subreddit.name})')
            unique.append(subreddit)
            i += 1

    return unique

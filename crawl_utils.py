import json

import jsons
import praw
import praw.models
from dataclasses import dataclass


def show_logs():
    import logging
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    logger = logging.getLogger('prawcore')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)


def get_reddit(should_show_logs=False):
    if should_show_logs:
        show_logs()
    login_data = LoginData.load()
    return praw.Reddit(
        client_id=login_data.client_id,
        client_secret=login_data.client_secret,
        user_agent=r'python:redditnet:0 (by /u/haluzpav)',
        username=login_data.username,
        password=login_data.password)


@dataclass
class LoginData:
    client_id: str
    client_secret: str
    username: str
    password: str
    _json_path: str = 'login.json'

    @classmethod
    def load(cls) -> 'LoginData':
        with open(cls._json_path) as file:
            d = json.load(file)
        o = jsons.load(d, cls)
        return o

import datetime
import json
import math
import os.path
from typing import Iterable, Set, List, Optional, Callable

import jsons
from dataclasses import dataclass, field

from utils import assert_file_dir, get_now


@dataclass
class Serializable:
    @classmethod
    def load(cls, path: str) -> Optional['Serializable']:
        if not os.path.exists(path):
            return None
        with open(path) as file:
            d = json.load(file)
        o = jsons.load(d, cls)
        # noinspection PyTypeChecker
        return o

    def save(self, path: str):
        assert_file_dir(path)
        d = jsons.dump(self, strip_privates=True, strip_properties=True, strip_nulls=True)
        with open(path, 'w') as file:
            json.dump(d, file, indent=2)


@dataclass
class Subreddit:
    name: str
    visits: int = 0
    visitors: Set[str] = field(default_factory=set)
    activity: float = None
    subscribers: int = None
    uniqueness: float = None

    def compute_activity(self, norm_visits, norm_visitors):
        self.activity = self._activity_manhattan(norm_visits, norm_visitors)

    def _activity_manhattan(self, norm_visits, norm_visitors):
        return sum((
            self.visits / norm_visits,
            len(self.visitors) / norm_visitors
            )) / 2

    def _activity_euclid(self, norm_visits, norm_visitors):
        return (sum(map(lambda i: i * i, (
            self.visits / norm_visits,
            len(self.visitors) / norm_visitors
            ))) / 2) ** 0.5

    def compute_uniqueness(self):
        assert self.subscribers is not None
        self.uniqueness = self._sigmoid()

    _poly_min = 1e3
    _poly_exp = -.25

    def _polynomial(self):
        return min((self.subscribers / self._poly_min) ** self._poly_exp, 1)

    _sig_log_start = 3
    _sig_log_end = 7
    _sig_center = (_sig_log_start + _sig_log_end) / 2
    _sig_width = _sig_log_end - _sig_log_start

    def _sigmoid(self):
        x = math.log10(self.subscribers) - self._sig_center
        return 1 / (1 + math.exp(4 * x / self._sig_width))

    @property
    def specificity(self):
        return self.activity * (1 if self.uniqueness is None else self.uniqueness)

    def __lt__(self, other):
        # just to make this class compatible with PriorityQueue
        # actual priority for the queue is handled separately
        return False


@dataclass
class Subreddits(Serializable):
    origins: List[str] = None
    redditors: Set[str] = None
    subreddits: List[Subreddit] = None
    reduce_function: str = None
    visits: int = None
    visitors: int = None
    data_fetch_start: datetime.datetime = None
    data_fetch_end: datetime.datetime = None

    def compute_activities(self, reduce_function: Callable[[Iterable], int]):
        self.reduce_function = reduce_function.__name__
        self.visits = reduce_function(map(lambda s: s.visits, self.subreddits))
        self.visitors = reduce_function(map(lambda s: len(s.visitors), self.subreddits))
        for subreddit in self.subreddits:
            subreddit.compute_activity(self.visits, self.visitors)
        self.subreddits = sorted(self.subreddits, key=lambda s: s.activity, reverse=True)

    def data_fetched(self):
        self.data_fetch_end = get_now()

    @classmethod
    def load(cls, path: str) -> Optional['Subreddits']:
        return super().load(path)
